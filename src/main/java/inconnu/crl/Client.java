package inconnu.crl;

import com.google.protobuf.ByteString;
import inconnu.crl.proto.add.AddServiceGrpc;
import inconnu.crl.proto.add.addRequest;
import inconnu.crl.proto.add.addResponse;
import inconnu.crl.proto.create.CreateServiceGrpc;
import inconnu.crl.proto.create.createRequest;
import inconnu.crl.proto.create.createResponse;
import inconnu.crl.proto.verify.VerifyServiceGrpc;
import inconnu.crl.proto.verify.verifyRequest;
import inconnu.crl.proto.verify.verifyResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.security.*;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

/**
 * Client Methods to ease the communication with the server
 */
public class Client {

    /** This method enables the creation of a pool CRL
     * @param managerCertificate The pool manager certificate
     * @param serverIP The CRL server ip
     * @param serverPort The CRL server port
     * @return The request result
     * @throws CertificateEncodingException If an exception occurs when encoding the certificate occurs it is thrown
     */
    public static String create(X509Certificate managerCertificate,String serverIP,int serverPort) throws CertificateEncodingException {
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress(serverIP,serverPort)
                .usePlaintext()
                .build();

        CreateServiceGrpc.CreateServiceBlockingStub createServiceBlockingStub = CreateServiceGrpc.newBlockingStub(managedChannel);

        createResponse createResponse = createServiceBlockingStub.create(createRequest.newBuilder()
                .setDomainManagerCert(ByteString.copyFrom(managerCertificate.getEncoded()))
                .build());

        while (!managedChannel.isShutdown()) managedChannel.shutdownNow();

        return createResponse.getStatus().toStringUtf8();
    }

    /**This method adds a certificate to the CRL of the pool where it belongs, this must be executed by the manager directly
     * @param managerCertificate The pool manager certificate
     * @param managerPrivateKey The ECDSA private key of the manager
     * @param certificateToRevoke The certificate to revoke
     * @param serverIP The CRL server ip
     * @param serverPort The CRL server port
     * @return The request result
     * @throws Exception If an error processing the signature or certificate encoding occurs an exception is thrown
     */
    public static String add(X509Certificate managerCertificate, PrivateKey managerPrivateKey, X509Certificate certificateToRevoke, String serverIP, int serverPort) throws Exception {
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress(serverIP,serverPort)
                .usePlaintext()
                .build();

        AddServiceGrpc.AddServiceBlockingStub addServiceBlockingStub = AddServiceGrpc.newBlockingStub(managedChannel);

        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        byte[] hash = messageDigest.digest(certificateToRevoke.getEncoded());

        Signature signature = Signature.getInstance("SHA256withECDSA");
        signature.initSign(managerPrivateKey);
        signature.update(hash);
        byte[] signRes =  signature.sign();


        addResponse response = addServiceBlockingStub.add(addRequest.newBuilder()
                .setDomainManagerCert(ByteString.copyFrom(managerCertificate.getEncoded()))
                .setCertToRevoke(ByteString.copyFrom(certificateToRevoke.getEncoded()))
                .setHashToRevokeSigned(ByteString.copyFrom(signRes))
                .build());


        while (!managedChannel.isShutdown()) managedChannel.shutdownNow();

        return response.getStatus().toStringUtf8();

    }

    /**This method adds a certificate to the CRL of the pool where it belongs, this can be performed by the endpoint device
     * @param managerCertificate The manager certificate
     * @param signedHash The hash of the certificate to revoke signed with the manager ECDSA private key
     * @param certificateToRevoke The certificate to revoke
     * @param serverIP The CRL server ip
     * @param serverPort The CRL server port
     * @return The request response
     * @throws Exception If an error processing the certificate happens an exception is thrown
     */
    public static String add(X509Certificate managerCertificate, byte[] signedHash, X509Certificate certificateToRevoke, String serverIP, int serverPort) throws Exception {
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress(serverIP,serverPort)
                .usePlaintext()
                .build();

        AddServiceGrpc.AddServiceBlockingStub addServiceBlockingStub = AddServiceGrpc.newBlockingStub(managedChannel);


        addResponse response = addServiceBlockingStub.add(addRequest.newBuilder()
                .setDomainManagerCert(ByteString.copyFrom(managerCertificate.getEncoded()))
                .setCertToRevoke(ByteString.copyFrom(certificateToRevoke.getEncoded()))
                .setHashToRevokeSigned(ByteString.copyFrom(signedHash))
                .build());

        while (!managedChannel.isShutdown()) managedChannel.shutdownNow();
        return response.getStatus().toStringUtf8();

    }

    /** This method is used to verify if the certificate was revoked
     * @param certificateToVerify The certificate to verify
     * @param serverIP The CRL server ip
     * @param serverPort The CRL server ip
     * @return The request response
     * @throws CertificateEncodingException If an error occurs on the encoding of the certificate an exception is thrown
     */
    public static String verify(X509Certificate certificateToVerify,String serverIP,int serverPort) throws CertificateEncodingException {
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress(serverIP,serverPort)
                .usePlaintext()
                .build();

        VerifyServiceGrpc.VerifyServiceBlockingStub verifyServiceBlockingStub = VerifyServiceGrpc.newBlockingStub(managedChannel);

        verifyResponse verify = verifyServiceBlockingStub.verify(verifyRequest.newBuilder()
                .setCertificateToVerify(ByteString.copyFrom(certificateToVerify.getEncoded()))
                .build());

        while (!managedChannel.isShutdown()) managedChannel.shutdownNow();
        return verify.getStatus().toStringUtf8();
    }


}
