package inconnu.crl;


import com.google.protobuf.ByteString;
import com.mongodb.ReadConcern;
import com.mongodb.WriteConcern;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import inconnu.crl.proto.add.AddServiceGrpc;
import inconnu.crl.proto.add.addRequest;
import inconnu.crl.proto.add.addResponse;
import inconnu.crl.proto.create.CreateServiceGrpc;
import inconnu.crl.proto.create.createRequest;
import inconnu.crl.proto.create.createResponse;
import inconnu.crl.proto.verify.VerifyServiceGrpc;
import inconnu.crl.proto.verify.verifyRequest;
import inconnu.crl.proto.verify.verifyResponse;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.bson.Document;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;

/**
 * This class instantiates a CRL server.<br>
 * The storage is done on a mongo database
 */
public class CRLServer {
    static String mongoURL;
    public static void main(String[] args){
        mongoURL = args[0];


        try {
            Server server = ServerBuilder.forPort(Configuration.SERVER_PORT)
                    .addService(new AddService())
                    .addService(new VerifyService())
                    .addService(new CreateService())
                    .build()
                    .start();
            server.awaitTermination();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }


    }

    /**
     * This service adds a certificate to the CRL of that pool
     * The protocol in place is described on the dissertation
     */
    public static class AddService extends AddServiceGrpc.AddServiceImplBase{
        MongoClient mongoClient;
        AddService(){
            mongoClient = MongoClients.create(mongoURL);
        }
        @Override
        public void add(addRequest request, StreamObserver<addResponse> responseObserver) {

            System.out.println("CRL received add request");

            try {
                X509Certificate managerCert = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(request.getDomainManagerCert().toByteArray()));
                X509Certificate certToRevoke = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(request.getCertToRevoke().toByteArray()));
                byte[] hashSignature = request.getHashToRevokeSigned().toByteArray();


                try {
                    certToRevoke.verify(managerCert.getPublicKey());
                } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchProviderException | SignatureException e) {
                    e.printStackTrace();
                    responseObserver.onNext(addResponse.newBuilder().setStatus(ByteString.copyFrom("certificate to revoke was not signed by the manager on the certificate".getBytes())).build());
                    System.out.println("CRL certificate to revoke was not signed by the manager on the certificate");
                    responseObserver.onCompleted();
                    return;
                }

                byte[] hash = MessageDigest.getInstance("SHA-256").digest(certToRevoke.getEncoded());

                Signature signature = Signature.getInstance("SHA256withECDSA");
                signature.initVerify(managerCert.getPublicKey());
                signature.update(hash);
                boolean verify = signature.verify(hashSignature);

                if (!verify){
                    responseObserver.onNext(addResponse.newBuilder().setStatus(ByteString.copyFrom("certificate hash not signed properly".getBytes())).build());
                    System.out.println("CRL certificate hash not signed properly");
                    responseObserver.onCompleted();
                    return;
                }

                String USSN = managerCert.getSubjectX500Principal().getName().split("CN=")[1];
                String domain = USSN.split("manager\\.")[1];


                MongoDatabase crl = mongoClient.getDatabase("crl");
                MongoCollection<Document> collection = crl.getCollection(domain).withWriteConcern(WriteConcern.MAJORITY).withReadConcern(ReadConcern.MAJORITY);
                Document managerCrt = collection.find(new Document("_id", "managercrt")).first();

                assert managerCrt != null;
                X509Certificate poolManagerCrt = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(Base64.getDecoder().decode(managerCrt.getString("crtB64"))));

                if (!poolManagerCrt.equals(managerCert)){
                    responseObserver.onNext(addResponse.newBuilder().setStatus(ByteString.copyFrom("Certificate that signed this certificate is not the pool manager".getBytes())).build());
                    System.out.println("Certificate that signed this certificate is not the pool manager");
                    responseObserver.onCompleted();
                    return;
                }


                Document add = new Document()
                        .append("subject",certToRevoke.getSubjectX500Principal().getName())
                        .append("signedHash",Base64.getEncoder().encodeToString(hashSignature))
                        .append("hash",Base64.getEncoder().encodeToString(hash));

                collection.insertOne(add);

                responseObserver.onNext(addResponse.newBuilder().setStatus(ByteString.copyFrom("success".getBytes())).build());
                responseObserver.onCompleted();

            } catch (CertificateException | NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
                e.printStackTrace();
                responseObserver.onNext(addResponse.newBuilder().setStatus(ByteString.copyFrom("exception occurred".getBytes())).build());
                responseObserver.onCompleted();
            }
        }
    }

    /**
     * This service enables the certificate verification
     * The protocol in place is described in the dissertation
     */
    public static class VerifyService extends VerifyServiceGrpc.VerifyServiceImplBase{
        MongoClient mongoClient;
        VerifyService(){
            mongoClient = MongoClients.create(mongoURL);
        }
        @Override
        public void verify(verifyRequest request, StreamObserver<verifyResponse> responseObserver) {

            System.out.println("CRL received verify");

            try {
                X509Certificate certToVerify = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(request.getCertificateToVerify().toByteArray()));

                String USSN = certToVerify.getIssuerX500Principal().getName().split("CN=")[1];
                String domain = USSN.split("manager\\.")[1];

                MongoDatabase crl = mongoClient.getDatabase("crl");
                MongoCollection<Document> collection = crl.getCollection(domain).withReadConcern(ReadConcern.MAJORITY);

                Document managerCertDoc = collection.find(new Document("_id", "managercrt")).first();

                assert managerCertDoc != null;
                X509Certificate managerCrt = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(Base64.getDecoder().decode(managerCertDoc.getString("crtB64"))));


                try {
                    if (!managerCrt.equals(certToVerify)) certToVerify.verify(managerCrt.getPublicKey());
                } catch (InvalidKeyException | NoSuchProviderException | SignatureException e) {
                    e.printStackTrace();
                    responseObserver.onNext(verifyResponse.newBuilder().setStatus(ByteString.copyFrom("certificate to be verified was not signed by the domain manager".getBytes())).build());
                    System.out.println("CRL certificate to be verified was not signed by the domain manager");
                    responseObserver.onCompleted();
                    return;
                }


                String hash = Base64.getEncoder().encodeToString(MessageDigest.getInstance("SHA-256").digest(certToVerify.getEncoded()));

                boolean found=false;
                ArrayList<Document> results = collection.find(Filters.eq("hash", hash)).into(new ArrayList<>());

                for(Document s : results){
                    String subject = s.getString("subject");
                    if (subject.equals(certToVerify.getSubjectX500Principal().getName())){
                        found=true;
                    }
                }

                if (found){
                    responseObserver.onNext(verifyResponse.newBuilder().setStatus(ByteString.copyFrom("found".getBytes())).build());
                }else responseObserver.onNext(verifyResponse.newBuilder().setStatus(ByteString.copyFrom("not found".getBytes())).build());

                responseObserver.onCompleted();
            } catch (NullPointerException|CertificateException | NoSuchAlgorithmException e) {
                responseObserver.onError(e);
                e.printStackTrace();
            }


        }
    }

    /**
     * This service allows creating CRL for pools
     * The protocol in place is described on the dissertation
     */
    public static class CreateService extends CreateServiceGrpc.CreateServiceImplBase{

        MongoClient mongoClient;
        CreateService(){
            mongoClient = MongoClients.create(mongoURL);
        }

        @Override
        public void create(createRequest request, StreamObserver<createResponse> responseObserver) {

            System.out.println("CRL received create ");

            try {
                X509Certificate managerCrt  = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(request.getDomainManagerCert().toByteArray()));
                String USSN = managerCrt.getSubjectX500Principal().getName().split("CN=")[1];
                String domain = USSN.split("manager\\.")[1];

                MongoDatabase crl = mongoClient.getDatabase("crl");

                crl.createCollection(domain);

                MongoCollection<Document> collection = crl.getCollection(domain).withWriteConcern(WriteConcern.MAJORITY);

                Document managerCrtDoc = new Document()
                        .append("_id","managercrt")
                        .append("crtB64",Base64.getEncoder().encodeToString(managerCrt.getEncoded()));

                collection.insertOne(managerCrtDoc);


                responseObserver.onNext(createResponse.newBuilder().setStatus(ByteString.copyFrom("success".getBytes())).build());
                responseObserver.onCompleted();
            } catch (CertificateException e) {
                e.printStackTrace();
            }


        }
    }





}
