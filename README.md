# CRL Server
**Description:**  Server to host the certificate revocation list

**Project Name:** Inconnu

**Position on FIWARE architecture:** IdM component

**Code Documentation Format:** javadoc
***

## Contents
* Compilation
* Execution

### Compilation
To compile this source it is needed to install the necessary dependencies. The code is built in java 8 and with maven as a dependency and compilation automation tool.

To install java and maven run the following:
```shell
sudo apt-get update
sudo apt-get install openjdk-8-jdk maven
```

To compile the sources do the following:

```shell
mvn clean package
```

The compilation result is a **jar** file located at: ```./target/crl-0.1.jar``` 

The compilation result contain a server and client methods to easily communicate with the server 
### Execution

To execute the CRL server do the following:
```shell
java -cp ./target/crl-0.1.jar inconnu.crl.CRLServer <MongoDBURL>
```